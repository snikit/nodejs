var expect = require('chai').expect;
var add = require('../src/add');

describe('add()', function() {
  it('should add nos', function() {
    var x = 1;
    var y = 10;
    var sum = x + y;

    expect(sum).to.be.equal(add(x, y));
  });

  it('should add -ve nos', function() {
    var x = -1;
    var y = -10;
    var sum = x + y;

    expect(sum).to.be.equal(add(x, y));
  });
});
